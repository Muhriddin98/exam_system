<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Answer extends Model
{
    protected $table = 'answers_table';

    public function type()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }
}
