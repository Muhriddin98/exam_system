<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;


/**
 * Class DriverLicence
 * @package App\Models
 *
 * @property string $code
 * @property string $name
 * @property string $soato
 */
class Region extends Model
{
    protected $table = 'tbl_states';

    protected $fillable = [
        'id', 'name',
    ];

}
