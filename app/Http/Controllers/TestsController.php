<?php


namespace App\Http\Controllers;


use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class TestsController extends Controller
{
    public function lists($subject,$section)
    {
        $quetions = Question::where('subject_id','=',$subject)
            ->where('section_id','=',$section)
            ->where('status','=',1)
            ->orderBy('id','desc')
            ->get();
        return view('tests.lists',['questions'=>$quetions]);
    }

    public function add_subject()
    {
        return view('tests.add_subject');
    }

    public function add_test()
    {
        $subjects = DB::table('subject_table')
            ->get()->toArray();
        return view('tests.add_tests', compact('subjects'));
    }

    public function store_subject(Request $request)
    {
        $subject = $request->input('subject');
        $id = DB::table('subject_table')
            ->insertGetId([
                'name' => $subject
            ]);
        if ($id > 0) {
            return redirect()->back()->with('success', 'Muvaffaqiyatli saqlandi');
        } else {
            return redirect()->back()->with('error', 'Xatolik!');
        }
    }

    public function store_test(Request $request)
    {
        $subject = $request->input('subject');
        $question = $request->input('question');
        $answerR = $request->input('answerR');
        $answer1 = $request->input('answer1');
        $answer2 = $request->input('answer2');
        $answer3 = $request->input('answer3');
        $filePath = '';
        if ($request->hasFile('file')){
            $extension = $request->file('file')->extension();
            $fileName = 'file.'.$extension;
            $destination = implode(DIRECTORY_SEPARATOR, ['files/'.time(), $fileName]);
            Storage::disk('local')->put($destination, $request->file('file')->getContent());
            $filePath = Storage::disk('local')->files('files/'.time())[0];
        }
        $id = DB::table('questions_table')
            ->insertGetId([
                'question' => $question,
                'reference' => $filePath,
                'subject_id' => (int)$subject
            ]);
        DB::table('answers_table')
            ->insert([
                'answer' => $answerR,
                'question_id' => $id,
                'has_right' => 1
            ]);
        DB::table('answers_table')
            ->insert([
                'answer' => $answer1,
                'question_id' => $id,
                'has_right' => 0
            ]);
        DB::table('answers_table')
            ->insert([
                'answer' => $answer2,
                'question_id' => $id,
                'has_right' => 0
            ]);
        DB::table('answers_table')
            ->insert([
                'answer' => $answer3,
                'question_id' => $id,
                'has_right' => 0
            ]);
        if ($id > 0) {
            return redirect()->back()->with('success', 'Muvaffaqiyatli saqlandi');
        } else {
            return redirect()->back()->with('error', 'Xatolik!');
        }
    }
}
