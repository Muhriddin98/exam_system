<?php


namespace App\Http\Controllers;

use App\Models\Region;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function sign_in()
    {
        $section = Section::all();
        $states =Region::all();
        return view('auth.register',compact('section','states'));
    }

    /**
     * Handle an authentication attempt.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'username' => ['required'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return redirect('/');
        }

        return back()->withErrors([
            'username' => 'Ma`lumotlar xato kiritildi',
        ]);
    }

    public function create_account(Request $request)
    {
        $name = $request->input('name');
        $surname = $request->input('surname');
        $login = $request->input('username');
        $pinfl = $request->input('password');
        $state_id = $request->input('region');
        $section_id = $request->input('section');
        $id = DB::table('users')
            ->insertGetId([
                'name' => $name,
                'surname' => $surname,
                'username' => $login,
                'password' => Hash::make($pinfl),
                'pinfl' => $pinfl,
                'state_id' => $state_id,
                'section_id' =>$section_id,
                'status' => 9
                ]);
        if ($id > 0) {
            $credentials = $request->validate([
                'username' => ['required'],
                'password' => ['required'],
            ]);
            if (Auth::attempt($credentials)) {
                $request->session()->regenerate();
                return redirect('/');
            }
        }

        return back()->withErrors([
            'username' => 'Kiritilgan jshshir oldindan mavjud',
        ]);
    }

    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
