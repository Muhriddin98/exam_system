<style>
    /* Custom CSS for styling */
    .section {
        background-color: #f0f0f0;
        border: 1px solid #ddd;
        padding: 20px;
        text-align: center;
        height: 400px;
        margin: 20px;
        border-radius: 10px;
        transition: transform 0.3s ease-in-out;
    }
    .section h3{
        padding-top:10px;
    }

    .section:hover {
        transform: scale(1.05);
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="section" id="section1">
                <a href="#">
                    <img style="height: 300px;" src="/assets/img/axborot.jpg" alt="people">
                    <h3>Axborot kommunikatsiya texnoligiyalari</h3>
                </a>

            </div>
        </div>
        <div class="col-md-4">
            <div class="section" id="section2">
                <a href="#">
                    <img style="height: 300px;" src="/assets/img/iq.png" alt="people">
                <h3>IQ (Intelektual salohiyat darajasi)</h3>

                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="section" id="section3">
                <a href="#">
                    <img style="height: 300px;" src="/assets/img/mt.jpg" alt="people">
                <h3>Kasbiy mutahassislik bo'yicha</h3>
                </a>
            </div>
        </div>
    </div>
</div>
<script>
    // JavaScript for hover effect
    const sections = document.querySelectorAll('.section');

    sections.forEach((section, index) => {
        section.addEventListener('mouseover', () => {
            section.style.backgroundColor = '#e0e0e0';
        });

        section.addEventListener('mouseout', () => {
            section.style.backgroundColor = '#f0f0f0';
        });
    });
</script>
