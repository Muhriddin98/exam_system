@extends('layouts.app')

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h3 style="text-align: center;">Test savollarini qo'shish bo'limi</h3>
            </div>
            <div class="card-body">
                <h5 class="card-title" style="text-align: center;">Savol qo'shish formasi</h5>
                <form class="row g-3" enctype="multipart/form-data" method="post" action="{{ route('store.test') }}">
                    @csrf
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">
                                <label for="subject" class="form-label">Savol qaysi yo'nalishga tegishli:</label>
                                <select required class="form-select" name="subject" id="subject">
                                    <option value=""></option>
                                    <option value="1">123</option>
                                </select>
                                <br>
                                <label for="file" class="form-label">Savolga rasm biriktirish</label>
                                <input type="file" class="form-control" id="file" name="file">
                            </div>
                            <div class="col-6">
                                <label for="question" class="form-label">Savol matnini kiriting:</label>
                                <textarea required name="question" id="question" cols="20" rows="5" class="form-control"></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-6">
                                <label for="answerR" class="form-label" style="color: darkgreen;">To'g'ri javobni kiriting</label>
                                <input required type="text" class="form-control" id="answerR" name="answerR">
                                <br>
                                <label for="answer1" class="form-label" style="color: maroon;">Noto'g'ri javobni kiriting (1)</label>
                                <input required type="text" class="form-control" id="answer1" name="answer1">
                            </div>
                            <div class="col-6">
                                <label for="answer2" class="form-label" style="color: maroon;">Noto'g'ri javobni kiriting (2)</label>
                                <input required type="text" class="form-control" id="answer2" name="answer2">
                                <br>
                                <label for="answer3" class="form-label" style="color: maroon;">Noto'g'ri javobni kiriting (3)</label>
                                <input required type="text" class="form-control" id="answer3" name="answer3">
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Saqlash</button>
                        <button type="reset" class="btn btn-secondary">Toazalash</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
