@extends('layouts.app')

@section('content')
    <div class="col-lg-12">

        <div class="card">
            <div class="card-header">
                <h3 style="text-align: center;">Test savollari ro'yxatini ko'rish bo'limi</h3>
            </div>
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered nowrap" style="margin-top:20px; width:100%;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nomi</th>
                        <th>Rasmi</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=1;?>
                    @foreach($questions as $question)
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $question->question }}</td>
                            <td><img src="{{ asset($question->reference) }}" alt="Image"> </td>
                            <td>
                                <a href="{!! url ('/section/list/edit/'.$question->id) !!}"> <button type="button" class="btn btn-round btn-success">{{ trans('app.Edit')}}</button></a>

                                <a url="{!! url('/section/list/delete/'.$question->id)!!}" class="sa-warning"> <button type="button" class="btn btn-round btn-danger dgr">{{ trans('app.Delete')}}</button></a>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
