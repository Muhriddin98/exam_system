@extends('layouts.app')

@section('content')
    <div class="col-lg-12">

        <div class="card">
            <div class="card-header">
                <h3 style="text-align: center;">Testlar uchun yo'nalish qo'shish bo'limi</h3>
            </div>
            <div class="card-body">
                <h5 class="card-title" style="text-align: center;">Yo'nalish qo'shish formasi</h5>
                <form class="row g-3" method="post" action="{{ route('store.subject') }}">
                    @csrf
                    <div class="col-12">
                        <label for="subject" class="form-label">Yo'nalish nomi kiriting:</label>
                        <input type="text" class="form-control" style="" id="subject" name="subject">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Saqlash</button>
                        <button type="reset" class="btn btn-secondary">Toazalash</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
