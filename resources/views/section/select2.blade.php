@extends('layouts.app')
@section('content')
    <style>
        /* Custom CSS for styling */
        .section {
            border: 1px solid #ddd;
            padding: 20px;
            text-align: center;
            height: 200px;
            margin: 20px;
            border-radius: 10px;
            transition: transform 0.3s ease-in-out;
        }
        .section h3{
            padding-top:10px;
        }

        .section:hover {
            transform: scale(1.05);
        }
    </style>
    <div class="container">
        <div class="row">
            @foreach($sections as $section)
                @php
                    // Generate a random background color in hexadecimal format
                    $randomColor = '#' . dechex(rand(0x000000, 0xFFFFFF));
                @endphp
            <div class="col-md-4">
                <a href="{!! url('/tests/list/3/'.$section->id)!!}">
                <div class="section"  style="background-color: {{$randomColor}};">
                    <h3>{{$section->name}}</h3>
                </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
    <script>
        // JavaScript for hover effect
        const sections = document.querySelectorAll('.section');

    </script>

@endsection
