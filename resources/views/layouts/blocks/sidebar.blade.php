<aside id="sidebar" class="sidebar" style="background: rgb(150,200,240);">

    <ul class="sidebar-nav" id="sidebar-nav">

    <li class="nav-item">
            <a class="nav-link " href="{{ route('dashboard') }}">
                <i class="bi bi-grid"></i>
                <span>Dashboard</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#users" data-bs-toggle="collapse" href="#">
                <i class="bi bi-people"></i><span>Foydalanuvchilar</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="users" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                <li>
                    <a href="{{ route('user.list') }}">
                        <i class="bi bi-circle"></i><span>Ro'yxatni ko'rish</span>
                    </a>
                </li>
                <li>
                    <a href="">
                        <i class="bi bi-circle"></i><span>Imtihon biriktirish</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#exams" data-bs-toggle="collapse" href="#">
                <i class="bi bi-bricks"></i><span>Imtihonlar</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="exams" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                <li>
                    <a href="{{ route('exam.list') }}">
                        <i class="bi bi-circle"></i><span>Ro'yxatni ko'rish</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('add.exam') }}">
                        <i class="bi bi-circle"></i><span>Imtihon yaratish</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#questions" data-bs-toggle="collapse" href="#">
                <i class="bi bi-list-check"></i><span>Test savollari</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="questions" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                <li>
                    <a href="{{ route('section.select') }}">
                        <i class="bi bi-circle"></i><span>Ro'yxatni ko'rish</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('add.subject') }}">
                        <i class="bi bi-circle"></i><span>Yangi yo'nalish qo'shish</span>
                    </a>
                </li>
            </ul>
        </li>

    </ul>

</aside>
