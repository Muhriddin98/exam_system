<footer id="footer" class="footer">
    <div class="copyright">
        &copy; Copyright <strong><span>{{ date('Y') }} yil</span></strong>. Barcha huquqlar himoyalangan
    </div>
    <div class="credits" style="font-weight: 700; text-transform: uppercase;">
        O'zgaroinspeksiya
    </div>
</footer>
