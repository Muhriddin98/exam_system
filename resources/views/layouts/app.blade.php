<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Test tizimi</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/img/favicon.png') }}"  rel="icon">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/bootstrap/css/bootstrap.min.css') }}"  rel="stylesheet">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/quill/quill.snow.css') }}" rel="stylesheet">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/quill/quill.bubble.css') }}" rel="stylesheet">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/simple-datatables/style.css') }}" rel="stylesheet">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/tabs/tabs.css') }}" rel="stylesheet">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">

    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/datatable/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/datatable/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/datatable/responsive.bootstrap4.min.css') }}" rel="stylesheet">
    <!-- my style files -->
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/css/color-style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/css/myStyle.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/css/tasks-style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/css/skins-modes.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ \Illuminate\Support\Facades\URL::to('assets/css/newStyle.css') }}" rel="stylesheet" type="text/css">

    @stack('styles')
</head>

<body>

<div class="container-fluid">

    @include('layouts.blocks.navbar')

    <div class="container-fluid page-body-wrapper"  style="min-height: 100vh; display: flex; flex-direction: column;">

        @include('layouts.blocks.sidebar')

        <div id="main" class="main body flex-grow-1 px-3">
            @yield('content')
        </div>
        <div style="margin-top: auto;">
        @include('layouts.blocks.footer')
        </div>
    </div>

</div>

<!-- Vendor JS Files -->
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/chart.js/chart.umd.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/echarts/echarts.min.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/quill/quill.min.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/php-email-form/validate.js') }}"></script>

<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/datatable/js/jquery.dataTables.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/datatable/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/datatable/js/dataTables.buttons.min.js') }}"></script>

<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/datatable/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/datatable/js//vfs_fonts.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/datatable/js/buttons.html5.min.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/datatable/js/buttons.print.min.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/datatable/js/buttons.colVis.min.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/datatable/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/vendor/datatable/datatable.js') }}"></script>


<!-- Template Main JS File -->
<script src="{{ \Illuminate\Support\Facades\URL::to('assets/js/main.js') }}"></script>

</body>

</html>
