<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Test tizimi</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/quill/quill.snow.css" rel="stylesheet">
    <link href="assets/vendor/quill/quill.bubble.css" rel="stylesheet">
    <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="assets/vendor/simple-datatables/style.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>

<main style="background: linear-gradient(90deg, deepskyblue, lightskyblue, deepskyblue);">
    <div class="container">
        @if (\Session::has('success'))
            <div class="d-flex align-items-center justify-content-center">
                    <span class="alert alert-success"
                          style="width: 100%; text-align: center; font-size: 18px;">{!! \Session::get('success') !!}</span>
            </div>
            <br>
        @elseif (\Session::has('error'))
            <div class="d-flex align-items-center justify-content-center">
                    <span class="alert alert-danger"
                          style="width: 100%; text-align: center; font-size: 18px;">{!! \Session::get('error') !!}</span>
            </div>
            <br>
        @endif
        <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

                        <div class="d-flex justify-content-center py-4">
                            <a href="index.html" class="logo d-flex align-items-center w-auto">
                                <img src="assets/img/logo.png" alt="">
                                <span class="d-none d-lg-block">Test tizimi</span>
                            </a>
                        </div><!-- End Logo -->

                        <div class="card mb-3">

                            <div class="card-body">

                                <div class="pt-4 pb-2">
                                    <h5 class="card-title text-center pb-0 fs-4">Ro'yxatdan o'tish oynasi</h5>
                                    <p class="text-center small">Shaxsiy ma'lumotlaringizni kiriting</p>
                                </div>

                                <form class="row g-3 needs-validation" action="{{ route('create') }}" method="post">
                                    @csrf
                                    <div class="col-12">
                                        <label for="yourName" class="form-label">Ismingiz</label>
                                        <input type="text" name="name" class="form-control" id="yourName" required>
                                        <div class="invalid-feedback">Iltimos ismingizni kiriting!</div>
                                    </div>

                                    <div class="col-12">
                                        <label for="yourEmail" class="form-label">Familiyangiz</label>
                                        <input type="text" name="surname" class="form-control" id="yourEmail" required>
                                        <div class="invalid-feedback">Iltimos familiyangizni kiriting!</div>
                                    </div>
                                    <div class="col-12 ">
                                            <label class="form-label" for="region">Viloyatni tanlang <label
                                                    class="text-danger">*</label>
                                            </label>
                                            <select name="region" class="region form-control" required>
                                                @if(!empty($states))
                                                    @foreach($states as $region)
                                                        <option
                                                            value="{{ $region->id }}">{{ $region->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                    </div>
                                    <div class="col-12 ">
                                        <label class="form-label" for="section">Bo'limni tanlang <label
                                                class="text-danger">*</label>
                                        </label>
                                        <select name="section" class="form-control" required>
                                            @if(!empty($section))
                                                @foreach($section as $region)
                                                    <option
                                                        value="{{ $region->id }}">{{ $region->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-12">
                                        <label for="yourUsername" class="form-label">Email</label>
                                        <div class="input-group has-validation">
                                            <input type="email" name="username" class="form-control" id="yourUsername" required>
                                            <div class="invalid-feedback">Elektron pochta manzilingizni kiriting.</div>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <label for="yourPassword" class="form-label">JShShIR</label>
                                        <input type="text" name="password" class="form-control" maxlength="14" minlength="14" id="yourPassword" required>
                                        <div class="invalid-feedback">Iltimos JShShIRingizni kiriting!</div>
                                    </div>

                                    <div class="col-12">
                                        <button class="btn btn-primary w-100" type="submit">Ro'yxatdan o'tish</button>
                                    </div>
                                    <div class="col-12">
                                        <p class="small mb-0">Ro'yxatdan o'tganmisiz? <a href="{{ route('login') }}">Tizimga kirish</a></p>
                                    </div>
                                </form>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>

    </div>
</main><!-- End #main -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/chart.js/chart.umd.js"></script>
<script src="assets/vendor/echarts/echarts.min.js"></script>
<script src="assets/vendor/quill/quill.min.js"></script>
<script src="assets/vendor/simple-datatables/simple-datatables.js"></script>
<script src="assets/vendor/tinymce/tinymce.min.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="assets/js/main.js"></script>

</body>

</html>
