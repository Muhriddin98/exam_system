<?php

return [
    'state' => [
        \App\Models\Customer::STATE_INACTIVE => 'Faol emas',
        \App\Models\Customer::STATE_ACTIVE => 'Faol',
        \App\Models\Customer::STATE_LIQUIDATED => 'Likvidatsiya qilingan',
        \App\Models\Customer::STATE_REMOVED => 'O\'chirilgan',
    ],
];
