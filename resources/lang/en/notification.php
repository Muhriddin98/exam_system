<?php

return [
    'expiring_inspection' => [
    'message' => '"O\'zagroinspeksiya"
Hurmatli mulkdor, Sizga tegishli texnikaning texnik ko\'rikdan o\'tish muddati :date-yilda tugashi ma\'lum qilinadi.',
        'status' => [
            \App\Models\Message::STATUS_CREATED => 'yaratilgan',
            \App\Models\Message::STATUS_SUBMITTED => 'jo\'natilgan',
            \App\Models\Message::STATUS_DELIVERED => 'yetkazilgan',
            \App\Models\Message::STATUS_DELIVERY_ERROR => 'yetkazilmadi',
            \App\Models\Message::STATUS_INCORRECT_PHONE => 'Telefon raqami xato',
            \App\Models\Message::STATUS_REJECTED => 'Rad etilgan',
            \App\Models\Message::STATUS_NO_ANSWER => 'Javob olinmadi',
            \App\Models\Message::STATUS_WHOOPS => 'Xatolik',
        ]
    ],
];
