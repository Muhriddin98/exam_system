<?php

use Illuminate\Support\Facades\Route;

Route::get('login', [\App\Http\Controllers\AuthController::class, 'login'])->name('login');
Route::get('sing-in', [\App\Http\Controllers\AuthController::class, 'sign_in'])->name('sign.in');
Route::post('authenticate', [\App\Http\Controllers\AuthController::class, 'authenticate'])->name('authenticate');
Route::post('create-account', [\App\Http\Controllers\AuthController::class, 'create_account'])->name('create');
Route::post('logout', [\App\Http\Controllers\AuthController::class, 'logout'])->name('logout');

// Section
Route::group(['prefix' => 'section', 'middleware' => 'auth'], function () {
    Route::get('/add', '\App\Http\Controllers\SectionController@create');
    Route::get('/list', '\App\Http\Controllers\SectionController@index');
    Route::post('/store', '\App\Http\Controllers\SectionController@store');
    Route::get('/list/delete/{id}', '\App\Http\Controllers\SectionController@destroy');
    Route::get('/list/edit/{id}', '\App\Http\Controllers\SectionController@edit');
    Route::post('/list/edit/update/{id}', '\App\Http\Controllers\SectionController@update');
    Route::get('/select','\App\Http\Controllers\SectionController@select')->name('section.select');
    Route::get('/select2','\App\Http\Controllers\SectionController@select2')->name('section.select2');

});

Route::middleware('auth')->group(function () {
    Route::get('/', [\App\Http\Controllers\UsersController::class, 'dashboard'])->name('dashboard');

    Route::group(['prefix'=>'users'], function () {
       Route::get('/list', [\App\Http\Controllers\UsersController::class, 'lists'])->name('user.list');
       Route::get('/list', [\App\Http\Controllers\UsersController::class, 'lists'])->name('user.list');
    });
    Route::group(['prefix'=>'exams'], function () {
        Route::get('/list', [\App\Http\Controllers\ExamsController::class, 'lists'])->name('exam.list');
        Route::get('/add-exam', [\App\Http\Controllers\ExamsController::class, 'add_exam'])->name('add.exam');
        Route::get('/take-exam', [\App\Http\Controllers\ExamsController::class, 'take_exam'])->name('take.exam');
    });
    Route::group(['prefix'=>'tests'], function () {
        Route::get('/list/{subject}/{section}', [\App\Http\Controllers\TestsController::class, 'lists'])->name('test.list');
        Route::get('/add-test', [\App\Http\Controllers\TestsController::class, 'add_test'])->name('add.test');
        Route::get('/edit-test/{id}', [\App\Http\Controllers\TestsController::class, 'edit_test'])->name('edit.test');
        Route::post('/store-test', [\App\Http\Controllers\TestsController::class, 'store_test'])->name('store.test');
        Route::get('/add-subject', [\App\Http\Controllers\TestsController::class, 'add_subject'])->name('add.subject');
        Route::post('/store-subject', [\App\Http\Controllers\TestsController::class, 'store_subject'])->name('store.subject');
    });
});
